$("#newsletter-subscribe").iziModal({
  'title':'Newsletter Signup',
  'headerColor': '#c52127',
  'borderBottom': false,
  'radius':0,
  'width':'320px',
  'padding':'20px'
});

$(document).on('click', '.subscribe', function (event) {

    event.preventDefault();
    $('#newsletter-subscribe').iziModal('open');

});
