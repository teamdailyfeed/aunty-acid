function revealSearch() {

  $('.site-header__search, .site-header__search-icon').on('click', function(e){
    e.preventDefault();
    $('.site-header__search-bar').slideToggle();
  });

}
revealSearch();
