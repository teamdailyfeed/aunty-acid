<?php

  get_header();
    while ( have_posts() ) : the_post();
    $thumbnail = getPostImage(get_the_ID(), 'full');
 ?>

 <main class="site-content">

   <section class="align--center page-intro">

             <div class="post__info">

               <h1 class="article-title news-title font--40px"><?php echo get_the_title(); ?></h1>
               <div class="divider"></div>

             </div>

         </section>

         <section class="article-content align--center">
           <div class="container">

             <?php echo the_content(); ?>

         </section>


  </main>

<?php
  endwhile;
  get_footer();

 ?>
