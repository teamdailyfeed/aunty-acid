<?php

  get_header();

 ?>

 <main class="site-content">

   <section class="page-intro align--center">




             <div class="post__info">

               <h1 class="article-title news-title font--40px">Search results for '<?php echo get_search_query() ?>'</h1>
               <div class="divider"></div>

             </div>



               <?php
                   if(have_posts()){

                     echo '<section class="align--center">

                       <div class="columns columns--4 container">';

                        while ( have_posts() ) : the_post();

                        $thumbnail = getPostImage(get_the_ID(), 'carousel');
                        $category = getPostDisplayCategory(get_the_ID());
                        $author = get_the_author();
                        $date = get_the_time('jS F Y');

                        echo '<a class="post post--small col" href="' . get_permalink() . '">
                            <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
                              <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
                            </div>
                            <div class="post__info">
                              <h1 class="news-title font--22px">' . get_the_title() . '</h1>
                              <div class="divider"></div>
                              <p class="post-data font--16px">By ' . $author . '</p>
                              <p class="post-data font--16px">' . $date . '</p>
                            </div>
                          </a>';

                        endwhile;

                  echo '</div></section>';

                }else{

                  echo '<p class="font--16px">Sorry, no posts found. Please try another category or use our search again.</p>';

                }
                ?>

         </section>

  </main>

<?php

  get_footer();

 ?>
