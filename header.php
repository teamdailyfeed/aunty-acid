<?php
  if(!$_GET['lazy']){
 ?>
<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
<?php do_action( 'dx_rtk_injection' ); ?>
<?php
if ( wp_is_mobile() ) {
  echo '<script type="text/javascript" src="//thor.rtk.io/2diY/TRag_hREQ_RWS7/jita.js?dfp=1" async defer></script>';
} else {
  echo '<script type="text/javascript" src="//thor.rtk.io/qhsG/4GpE_5cSn_6K4r_YHoj/jita.js?dfp=1" async defer></script>';
}
?>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60092562-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-60092562-1');
</script>


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Qsbnm1akKd60i+", domain:"dailyfeed.co.uk",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Qsbnm1akKd60i+" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="post_id" content="<?php $postid = get_the_ID(); echo $postid?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<meta property="fb:pages" content="1547141465534084" />
<?php wp_head(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/main.css">

<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url'); ?>/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url'); ?>/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_url'); ?>/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,900" rel="stylesheet">

  <!-- Place the following code in the HEAD -->
  <script type="text/javascript">
    function detectWidth() {
      var w = window,
        d = document,
        e = d.documentElement,
        x = Math.max(e.offsetWidth,e.clientWidth);
      return x;
    }
  </script>
  <!-- End of head code -->


<script>
if(typeof Admanagement !== 'undefined'){
	var admanagement = new Admanagement({'endpoint': "http://d342r80tvdrdhm.cloudfront.net", 'trackEndpoint': 'http://prod-trac-ElasticL-5LD1TFK951UW-1769026700.us-east-1.elb.amazonaws.com', 'debug': true, 'adParams': { "utm_source": "default_source", "utm_device": null, "utm_campaign": "<default value>" }});admanagement.injectAdsIntoBody(["head", "ad-header", "left-sidebar", "right-sidebar", "in-article-top", "in-article-middle", "in-article-bottom", "ad-header-2", "in-article-footer"]);
}
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1075892245813054'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1075892245813054&ev=PageView&noscript=1"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?>>

  <header class="site-header">

  		<div class="site-header__top">
  			<a class="site-header__logo" href="/">
  				<?php include "assets/svgs/logo.svg" ?>
  			</a>

  			<a class="button pre-title font--15px font--white subscribe" href="#">Subscribe to our newsletter today!</a>

			</div>

  		<div class="site-header__mobile">
				<a class="site-header__mobile-logo" href="/">
					<?php include "assets/svgs/logo.svg" ?>
				</a>

  			<a class="button pre-title font--15px font--white subscribe" href="#">Subscribe to our newsletter today!</a>

  			<a class="site-header__search-icon" href="#">
  				<?php include "assets/svgs/search.svg" ?>
  			</a>

  			<a class="site-header__menu" href="<?php bloginfo('template_url'); ?>">
  				<span></span>
  			</a>
  		</div>

  		<nav class="site-header__nav">
				<a class="site-header__nav-logo" href="/">
					<?php include "assets/svgs/logo.svg" ?>
				</a>

  			<ul class="list--blank">

  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/news/">News</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/entertainment/">Entertainment</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/sport/">Sport</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/money/">Money</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/health/">Health</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/food/">Food</a></li>
  				<li class="site-header__item"><a class="site-header__link pre-title font--18px" href="/category/tech/">Tech</a></li>

  			</ul>

  			<div class="site-header__socials">
  				<a href="#" class="site-header__search">
  					<p class="post-data font--12px">Search</p>
  					<?php include "assets/svgs/search.svg" ?>
  				</a>

  				<a href="https://www.facebook.com/officialdailyfeed/" target="_blank" class="social social--fb">
  					<?php include "assets/svgs/facebook.svg" ?>
  				</a>


  			</div>
  		</nav>

  		<div class="site-header__search-bar">
  			<form action="/" method="GET" class="site-header__search-form">
  				<input class="site-header__search-input post-data font--16px" name="s" placeholder="Search articles">
  				<button class="site-header__search-btn page-title" type="submit" value="Search">Search</button>
  			</form>
  		</div>
  	</header><!-- site-menu -->
<?php
}
?>
